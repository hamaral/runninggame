﻿using UnityEngine;
using System.Collections;

public class SoundManager : Singleton<SoundManager> {

	public AudioSource efxSource;                   //Drag a reference to the audio source which will play the sound effects.
    public AudioSource musicSource;                 //Drag a reference to the audio source which will play the music.
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlaySingle(AudioClip clip, Transform instigator)
    {
		if(instigator != null)
		{
			GameManager.Instance.PlaySoundToZombies(instigator);
		}

		if(efxSource != null)
		{
			efxSource.clip = clip;
			efxSource.Play ();
		}
		
	}
}
