﻿using UnityEngine;
using System.Collections;

public class DoorTileType : InteractableTile {

	public float m_Health = 10f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void UseKey()
	{

		PlayerTeamController.Instance.UseKey();
		this.m_IsWakable = true;
		this.DestroyMe();
	}

	public void TakeDamage(float damage)
    {
        
		this.m_Health -= damage;
		if(this.m_Health <= 0)
		{
			this.DestroyMe();
		}
        
    }

	 private void DestroyMe()
    {
        Destroy(base.gameObject);
    }
}
