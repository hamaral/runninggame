using UnityEngine;
using System.Collections;

public enum E_WallTileType {final, ligacao, ligacaoT, curva};

public class WallTileType : TileType {

	public E_WallTileType m_WallType;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public E_WallTileType WallType
	{
		get{ return this.m_WallType;}
	}
}
