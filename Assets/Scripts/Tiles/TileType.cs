﻿using UnityEngine;
using System.Collections;

public enum E_TileType {grass, wall, floor, street, door};

public class TileType : MonoBehaviour {

	public bool m_IsWakable = true;
	public E_TileType m_TileType;
	public Int2 m_Pos;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public E_TileType Type
	{
		get{ return this.m_TileType;}
	}

	public bool IsWalkable
	{
		get {return this.m_IsWakable;}
	}
}
