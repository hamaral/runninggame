using UnityEngine;
using System.Collections.Generic;

public class NodePath
{
	private List<Vector2> waypoints = new List<Vector2>();
	private int next = 0;
	public bool draw;

	public int Next
	{
		get { return next; }
		set { next = value; }
	}

	public List<Vector2> Waypoints
	{
		get { return waypoints; }
		set { waypoints = value; }
	}

	private bool looped;


	public bool Looped
	{
		get { return looped; }
		set { looped = value; }
	}
		

	public Vector2 NextWaypoint()
	{
		return waypoints[next];
	}

	public bool IsLast()
	{
		return (next == waypoints.Count - 1);
	}

	public void AdvanceToNext()
	{
		if (looped)
		{
			next = (next + 1) % waypoints.Count;
		}
		else
		{
			if (next != waypoints.Count - 1)
			{
				next = next + 1;
			}
		}
	}
}