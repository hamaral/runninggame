using UnityEngine;
using System;

public class Node : IComparable<Node>
{
	public Vector2 pos;
	public float f, g, h;
	public Node parent;

	public int CompareTo(Node b)
	{
		if ((this.f < b.f))
		{
			return -1;
		}
		if (this.f == b.f)
		{
			return 0;
		}
		return 1;
	}
}