using System;
using System.Collections.Generic;
using UnityEngine;

public class AStar {


	Dictionary<Vector2, Node> open = new Dictionary<Vector2, Node>();

	Dictionary<Vector2, Node> closed = new Dictionary<Vector2, Node>();

    private ArrayTileType[]  m_Grid;

	Vector2 start, end;

	public NodePath Search(Vector2 startNode, Vector2 endNode) 
	{
		bool found = false;
		this.end = startNode; // end refers to start
		this.start = endNode; // start refers to end
		open.Clear();
        closed.Clear();

		Node first = new Node();
		first.f = first.g = first.h = 0.0f;
		first.pos = this.start;

		open[this.start] = first;

		Node current = first;
		int maxSize = 0;
		while (open.Count > 0)
		{

			if (open.Count > maxSize)
			{
				maxSize = open.Count;
			}

			// Get the top of the q
			float min = float.MaxValue;

			foreach (Node node in open.Values)
			{
				if (node.f < min)
				{
					current = node;
					min = node.f;
				}
			}

			if (current.pos.Equals(this.end))
			{
				found = true;
				break;
			}

			addAdjacentNodes(current);
			open.Remove(current.pos);

			closed[current.pos] = current;
		}

		NodePath path = new NodePath();
		if (found)
		{
			while (!current.pos.Equals(this.start))
			{
				path.Waypoints.Add(current.pos);
				current = current.parent;
			}
			path.Waypoints.Add(current.pos);

		}

		return path;
	}

	private void addAdjacentNodes(Node current)
	{
		//List<ArrayInt> map = WayPointCircuit.Instance.Grid;
		int w = this.m_Grid.Length;
		int h = this.m_Grid[0].Length;
		//frente
		if(h > (int)current.pos.y + 1 && this.m_Grid[(int)current.pos.x][(int)current.pos.y + 1].IsWalkable) 
		{
			AddIfValid(new Vector2(current.pos.x, current.pos.y + 1), current);
		}
		//esquerda
		if((int)current.pos.x > 0 &&  this.m_Grid[(int)current.pos.x -1][(int)current.pos.y].IsWalkable) 
		{
			AddIfValid(new Vector2(current.pos.x - 1, current.pos.y), current);
		}
		//direita
		if(w >  (int)current.pos.x + 1 && this.m_Grid[(int)current.pos.x + 1][(int)current.pos.y].IsWalkable) 
		{
			AddIfValid(new Vector2(current.pos.x + 1, current.pos.y), current);
		}

		//tras
		if((int)current.pos.y > 0 &&  this.m_Grid[(int)current.pos.x][(int)current.pos.y -1].IsWalkable) 
		{
			AddIfValid(new Vector2(current.pos.x, current.pos.y - 1), current);
		}
	}

	private void AddIfValid(Vector2 pos, Node parent)
	{
		if (!closed.ContainsKey(pos))
		{
			if (!open.ContainsKey(pos))
			{
				Node node = new Node();
				node.pos = pos;
				node.g = parent.g + cost(node.pos, parent.pos);
				node.h = heuristic(pos, end);
				node.f = node.g + node.h;
				node.parent = parent;
				open[pos] = node;
			}
			else
			{
				Node node = open[pos];
				float g = parent.g + cost(node.pos, parent.pos);
				if (g < node.g)
				{
					node.g = g;
					node.f = node.g + node.h;
					node.parent = parent;
				}
			}
		}
	}

	private float heuristic(Vector3 v1, Vector3 v2)
	{
		return 10.0f * (Math.Abs(v2.x - v1.x) + Math.Abs(v2.y - v1.y) + Math.Abs(v2.z - v1.z));
	}

	private float cost(Vector3 v1, Vector3 v2)
	{
		int dist = (int)Math.Abs(v2.x - v1.x) + (int)Math.Abs(v2.y - v1.y) + (int)Math.Abs(v2.z - v1.z);
		return (dist == 1) ? 10 : 14;
	}

    public void SetGrid(ArrayTileType[] grid)
    {
        this.m_Grid = grid;
    }
}