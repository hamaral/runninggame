﻿using System;
using System.Collections;
using System.Collections.Generic;

public static class MessageCenter
{
	private static Dictionary<string, Action<Message>> listeners = new Dictionary<string, Action<Message>>();
	private static Queue<Message> waitingMessages = new Queue<Message>();

	public static void AddListener(string messageID, Action<Message> callback)
	{
		if (listeners.ContainsKey(messageID))
		{
			Action<Message> a = listeners[messageID];
			Delegate.Combine (a, callback);
		}
		else
		{
			listeners.Add(messageID, callback);
		}
	}

	public static void Initialize()
	{
		Timer.AttachToUpdate(new System.Action(MessageCenter.PurgeQueue));
	}

	private static void PurgeQueue()
	{
		if (MessageCenter.waitingMessages.Count != 0)
		{
			ICollection waitingMessages = MessageCenter.waitingMessages;
			object syncRoot = waitingMessages.SyncRoot;
			lock (syncRoot)
			{
				Message[] array = new Message[waitingMessages.Count];
				waitingMessages.CopyTo(array, 0);
				MessageCenter.waitingMessages.Clear();
				for (int i = 0; i != array.Length; i++)
				{
					PushMessage(array[i]);
				}
			}
		}
	}

	public static void PushMessage(Message message)
	{
		if (listeners.ContainsKey(message.message))
		{
			Action<Message> action = listeners[message.message];
			if (action != null)
			{
				action(message);
			}
		}
	}

	public static void PushMessage(string messageID)
	{
		Hashtable parameters = new Hashtable();
		PushMessage(messageID, parameters);
	}

	public static void PushMessage(string messageID, Hashtable parameters)
	{
		if (listeners.ContainsKey(messageID))
		{
			Action<Message> action = listeners[messageID];
			if (action != null)
			{
				Message message = new Message(messageID, parameters);
				action(message);
			}
		}
	}

	public static void PushMessageParams(string messageID, params object[] parameters)
	{
		Hashtable hashtable = new Hashtable();
		if (parameters != null)
		{
			if ((parameters.Length % 2) != 0)
			{
				return;
			}
			for (int i = 0; i < parameters.Length; i += 2)
			{
				hashtable.Add(parameters[i], parameters[i + 1]);
			}
		}
		PushMessage(messageID, hashtable);
	}

	public static void PushMessageTuples(string messageID, params Tuple<string, object>[] parameters)
	{
		Hashtable hashtable = new Hashtable();
		if (parameters != null)
		{
			foreach (Tuple<string, object> tuple in parameters)
			{
				hashtable.Add(tuple.Item1, tuple.Item2);
			}
		}
		PushMessage(messageID, hashtable);
	}

	public static void QueueMessage(Message message)
	{
		object syncRoot = ((ICollection) waitingMessages).SyncRoot;
		lock (syncRoot)
		{
			waitingMessages.Enqueue(message);
		}
	}

	public static void RemoveListener(string messageID, Action<Message> callback)
	{
		if (listeners.ContainsKey(messageID))
		{
			listeners.Remove (messageID);
		}
	}
}

