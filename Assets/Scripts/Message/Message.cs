﻿using System;
using System.Collections;
using System.Text;

public class Message
{
	public Message()
	{
		this.message = string.Empty;
		this.parameters = null;
	}

	public Message(string mess)
	{
		this.message = mess;
		this.parameters = null;
	}

	public Message(string mess, Hashtable param)
	{
		this.message = mess;
		this.parameters = param;
	}

	public void AddParameter(string param, object value)
	{
		if (this.parameters == null)
		{
			this.parameters = new Hashtable();
		}
		if (this.parameters.ContainsKey(param))
		{
			this.parameters[param] = value;
		}
		else
		{
			this.parameters.Add(param, value);
		}
	}

	public void Fire()
	{
		MessageCenter.PushMessage(this);
	}

	public T GetParameter<T>(string key)
	{
		if ((this.parameters != null) && this.parameters.ContainsKey(key))
		{
			return (T) this.parameters[key];
		}
		return default(T);
	}

	public bool HasParameter(string key) { 
		return ((this.parameters != null) && this.parameters.ContainsKey (key));
	}

	public void Queue()
	{
		MessageCenter.QueueMessage(this);
	}

	public override string ToString()
	{
		StringBuilder builder = new StringBuilder();
		builder.AppendLine("[" + this.message + "]");
		if (this.parameters != null)
		{
			IDictionaryEnumerator enumerator = this.parameters.GetEnumerator();

			while (enumerator.MoveNext())
			{
				DictionaryEntry current = (DictionaryEntry) enumerator.Current;
				builder.AppendLine(string.Concat(new object[] { "{ ", current.Key, " -> ", current.Value, " ( ", current.Value.GetType(), " ) }" }));
			}

		}
		return builder.ToString();
	}

	public string message { get; private set; }

	public Hashtable parameters { get; private set; }
}

