﻿using UnityEngine;
using System.Collections;

public class CameraController : Singleton<CameraController> {
	
	//camera target
	public Transform cameraTarget;	
	//camera trasform
	private Transform myTransform;	
	//camera offset
	public Vector3 cameraOffset;	
	[SerializeField]private float m_ZoomMin;
	[SerializeField]private float m_ZoomMax;

	private float zoomSpeed = 2.0f;
	private float m_CurrentZoom;
	private float m_TargetZoom;


	void Awake()
	{
		this.myTransform = this.transform;		
	}

	void Start()
	{
		m_CurrentZoom =  m_ZoomMin;
		m_TargetZoom =  m_ZoomMin;
	}
	
	void LateUpdate()
	{
		this.UpdateCamera();
	}
	
	private void UpdateCamera()
	{
		if (!this.cameraTarget)
		{
			return;
		}

		Vector3 desiredPosition = this.cameraTarget.transform.position + this.cameraOffset;	
		//desiredPosition.y = m_CurrentZoom;
		this.myTransform.position = desiredPosition;		
		this.myTransform.LookAt(this.cameraTarget.position);
	}

	private IEnumerator ZoomInOutCamera()
	{
		float difCameraZoom = Mathf.Abs(m_CurrentZoom - m_TargetZoom);

		while(difCameraZoom > 0.5f)
		{
			m_CurrentZoom = Mathf.Lerp (m_CurrentZoom, m_TargetZoom, Time.deltaTime * zoomSpeed);
			difCameraZoom = Mathf.Abs(m_CurrentZoom - m_TargetZoom);
			yield return null;
		}

		m_CurrentZoom = m_TargetZoom;

	}

	public void ToggleZoom()
	{

		if (m_CurrentZoom > m_ZoomMin)
		{
			m_TargetZoom = m_ZoomMin;
		}
		else
		{
			m_TargetZoom = m_ZoomMax;
		}

		StartCoroutine(ZoomInOutCamera());


	}
}
