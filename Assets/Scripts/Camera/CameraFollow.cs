using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    public Transform m_Target;
    public float m_OffsetZ;
    public float m_OffsetY;
	// Use this for initialization
	void Start () {
		
	}


    void LateUpdate()
    {
        if(this.m_Target != null)
        {
            Vector3 pos = this.m_Target.position;
            pos.x = this.m_Target.position.x;
            pos.y = this.m_OffsetY;
           // pos.z =  this.m_Target.position.z + this.m_OffsetZ; 
            //base.transform.position = pos;
            base.transform.LookAt(pos);

        }
    }
	


	
}
