﻿using UnityEngine;
using System.Collections;
using System;

public class InputController : MonoBehaviour {

	private CharacterSelect m_CurrentCharacterSelected;
	public float m_HoldTimeShowUI = 1.0f;
	private float m_LastTimeHold = 0f;

	private bool m_IsInteractMenuOpen = false;
	// Use this for initialization
	void Start () {
	
	}
	
	protected virtual void OnDisable()
	{
		this.CleanupMessages();
	}

	protected virtual void OnEnable()
	{
		this.SetupMessages();
	}

	private void CleanupMessages()
	{
		MessageCenter.RemoveListener("OnOpenMenu", new Action<Message>(this.OnOpenMenu));
		MessageCenter.RemoveListener("OnCloseMenu", new Action<Message>(this.OnCloseMenu));
	}

	private void SetupMessages()
	{
		
		MessageCenter.AddListener("OnOpenMenu", new Action<Message>(this.OnOpenMenu));
		MessageCenter.AddListener("OnCloseMenu", new Action<Message>(this.OnCloseMenu));
		
	}

	public void OnOpenMenu(Message message)
	{
		this.m_IsInteractMenuOpen = true;		
	}

	public void OnCloseMenu(Message message)
	{
		this.m_IsInteractMenuOpen = false;
		
	}

	// Update is called once per frame
	void Update () {

		 if(Input.GetMouseButtonDown(0)){
			 this.m_LastTimeHold = Time.time;
			 Vector3 mousePos = Input.mousePosition;         	
			 Vector2 worldTouch = Camera.main.ScreenToWorldPoint(mousePos);			
			 Collider2D col = Physics2D.OverlapPoint(worldTouch); 

			 if(col != null){
				 
				 if((col.tag == "Floor"  || col.tag == "Grass") && this.m_CurrentCharacterSelected != null && !this.m_IsInteractMenuOpen)
				 {	
					NodePath p = BoardManager.Instance.GeneratePath(this.m_CurrentCharacterSelected.transform.position, col.transform.position);					
					AIController ai = this.m_CurrentCharacterSelected.GetComponent<AIController>();
					ai.MoveTo(p);
					 
				 }
				
				CharacterSelect cs = col.GetComponent<CharacterSelect>();
				if(cs != null){
					this.m_CurrentCharacterSelected = cs;
					this.m_CurrentCharacterSelected.SelectCharacter();
				}
					 
			 }
		} else if(Input.GetMouseButton(0)){
			
			if(Time.time - this.m_LastTimeHold >= this.m_HoldTimeShowUI)
			{
				Vector3 mousePos = Input.mousePosition;         	
				Vector2 worldTouch = Camera.main.ScreenToWorldPoint(mousePos);			
				Collider2D col = Physics2D.OverlapPoint(worldTouch); 
				if(col != null && this.m_CurrentCharacterSelected != null){
					if(col.tag == "Door")
					{
						Tuple<string, object>[] parameters = new Tuple<string, object>[] 
						{ 
							Tuple.Create<string, object>("Door", col.gameObject), 						
							Tuple.Create<string, object>("Character", this.m_CurrentCharacterSelected.gameObject)
						};
						MessageCenter.PushMessageTuples("OnOpenDoorMenu", parameters);
					}
				}
			}

			
		}
		 else if(Input.GetMouseButtonUp(0)){
			
			m_LastTimeHold = 0;
		}
	}
}
