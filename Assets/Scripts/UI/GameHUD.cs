﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameHUD : MonoBehaviour {

	public Text m_TotalSurvivorText;

	private int m_TotalSurvivorOld = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(this.m_TotalSurvivorOld != GameManager.Instance.TotalSurvivorRescued)
		{
			this.m_TotalSurvivorOld = GameManager.Instance.TotalSurvivorRescued;
			this.UpdateTotalSurvivors();
		}
	}

	private void UpdateTotalSurvivors()
	{
		m_TotalSurvivorText.text = "Survivor Rescued: " + this.m_TotalSurvivorOld;
	}
}
