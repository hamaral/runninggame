﻿using UnityEngine;
using System.Collections;
using System;

public class MenuDoor : Singleton<MenuDoor> {

	public GameObject m_KeyButton;
	public GameObject m_ShootButton;

	public GameObject m_Panel;

	private bool m_IsOpen;
	private GameObject m_CurrentDoor;
	private GameObject m_Character;

	private void CleanupMessages()
	{
		MessageCenter.RemoveListener("OnOpenDoorMenu", new Action<Message>(this.OnOpenDoorMenu));
	}

	private void SetupMessages()
	{
		
		MessageCenter.AddListener("OnOpenDoorMenu", new Action<Message>(this.OnOpenDoorMenu));
		
	}

	// Use this for initialization
	void Start () {
		m_IsOpen = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(this.m_IsOpen && this.m_CurrentDoor != null)
		{
			Vector3 worldPos = new Vector3(this.m_CurrentDoor.transform.position.x, this.m_CurrentDoor.transform.position.y + 1f, 0f);
			Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
			m_Panel.transform.position = new Vector3(screenPos.x, screenPos.y, 0);
		}
	
	}

	protected virtual void OnDisable()
	{
		this.CleanupMessages();
	}

	protected virtual void OnEnable()
	{
		this.SetupMessages();
	}

	public void OnOpenDoorMenu(Message message)
	{
		if(!this.m_IsOpen)
		{
			MessageCenter.PushMessageTuples("OnOpenMenu", null);
			this.m_IsOpen = true;
			this.m_CurrentDoor = message.GetParameter<GameObject>("Door");;
			this.m_Character = message.GetParameter<GameObject>("Character");;
			this.m_Panel.SetActive(true);
			if(PlayerTeamController.Instance.TotalKeys <= 0)
			{
				this.m_KeyButton.SetActive(false);
			}
		}
		
	}

	public void CloseButton()
	{
		MessageCenter.PushMessageTuples("OnCloseMenu", null);
		this.m_IsOpen = false;
		this.m_CurrentDoor = null;
		this.m_Character = null;
		this.m_Panel.SetActive(false);
	}

	public void ShootDoor()
	{
		if(this.m_Character != null)
		{
			this.m_Character.GetComponent<AIController>().ShootTarget(this.m_CurrentDoor.transform);
			this.CloseButton();
		}
	}

	public void UseKeyInDoor()
	{
		this.m_CurrentDoor.GetComponent<DoorTileType>().UseKey();
		this.CloseButton();
	}
}
