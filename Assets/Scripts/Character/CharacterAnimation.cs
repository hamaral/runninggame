using UnityEngine;
using System.Collections;

[RequireComponent(typeof (CharacterController2D))]
public class CharacterAnimation : MonoBehaviour
{
    [SerializeField]private Sprite m_IdleSprite;
    [SerializeField]private Sprite m_WalkSprite;
    
    private Sprite m_CurrentSprite;    
    private SpriteRenderer m_SpriteRenderer;    
    private CharacterController2D m_Character;
 
    void Awake()
    {
        this.m_Character = base.GetComponent<CharacterController2D>();
        this.m_SpriteRenderer = base.GetComponentInChildren<SpriteRenderer>();
    }
    
    void Update()
    {
        if(this.m_Character.CurrentSpeed > 0.01f && this.m_CurrentSprite != m_WalkSprite)
        {
            this.m_CurrentSprite = this.m_WalkSprite;
            this.m_SpriteRenderer.sprite = m_WalkSprite;
        }
    }
}