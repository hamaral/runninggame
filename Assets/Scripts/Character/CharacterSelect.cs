﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterSelect : MonoBehaviour {

	[SerializeField]private GameObject m_SpriteSelected;

	// Use this for initialiation
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SelectCharacter()
	{
		this.m_SpriteSelected.SetActive(true);
	}

	public void DeselectCharacter()
	{
		this.m_SpriteSelected.SetActive(false);
	}
}
