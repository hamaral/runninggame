using UnityEngine;
using System.Collections;

public class CharacterController2D : MonoBehaviour
{
    [SerializeField]
    private float m_WalkSpeed = 1.0f;    
    private float m_CurrentSpeed = 0f;
    [SerializeField]private float m_Health = 10.0f;
    public LayerMask m_BlockingLayer; 

    private Rigidbody2D m_rigidBody;
    
    private bool m_bIsAlive = false;

    private Weapon m_Weapon;
    
    private Transform m_Transform;
    
    void Awake()
    {
        this.m_Transform = base.transform;
        this.m_rigidBody = base.GetComponent<Rigidbody2D>();
        this.m_Weapon = base.GetComponentInChildren<Weapon>();
    }

    void Start()
    {
        this.m_bIsAlive = true;
    }
    void Update()
    {
        
    }
    
    public void RotateBody(float a)
    {
        this.m_rigidBody.MoveRotation(a);
    }

    public void RotateBody(Quaternion q)
    {
        this.m_Transform.rotation = q;
    }

    public void Move(float moveForward, float moveSide)
    {
        this.m_CurrentSpeed = moveForward;
        if (Mathf.Abs (moveForward) > 0.01f || Mathf.Abs (moveSide) > 0.01f) 
        {
            this.m_rigidBody.velocity = new Vector2 (this.m_WalkSpeed * moveSide, this.m_WalkSpeed * moveForward);            
        }
    }

    public void MoveTo(float xDir, float yDir)
    {
        Vector2 end = new Vector2 (xDir, yDir);
        SmoothMovement (end);
    }

    protected void SmoothMovement (Vector3 end)
    {
       Vector3 newPostion = Vector3.MoveTowards(this.m_rigidBody.position, end, this.m_WalkSpeed * Time.deltaTime);
       this.m_rigidBody.MovePosition (newPostion);
          
    }

    public void TakeDamage(float damage)
    {
        if(this.m_bIsAlive)
        {
            this.m_Health -= damage;
            if(this.m_Health <= 0)
            {
                this.Die();
            }
        }
    }

    public void StopMove()
    {
        
        this.m_rigidBody.angularVelocity = 0;
        this.m_rigidBody.velocity = Vector3.zero;
        StopCoroutine("SmoothMovement");
        
    }

    public void FireWeapon()
    {
        if(this.m_Weapon != null){
            this.m_Weapon.Fire();
        }
    }

    private void Die()
    {
        Destroy(base.gameObject);
    }
    
    public float CurrentSpeed
    {
        get { return this.m_CurrentSpeed;}
    }

    public Weapon Weapon
    {
        get{return this.m_Weapon;}
    }
    
}