using UnityEngine;
using System.Collections;

public class PlayerTeamController : Singleton<PlayerTeamController> {


    private int m_TotalKeys = 0;
    private AIController[] m_Characters;

	// Use this for initialization
	void Start () {
	}
	


	void OnDrawGizmos() {
      
    }

    public void UseKey()
    {
        this.m_TotalKeys--;
    }

    public int TotalKeys
    {
        get{return this.m_TotalKeys;}
    }
}
