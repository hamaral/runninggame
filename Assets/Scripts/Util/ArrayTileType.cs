using System;
using UnityEngine;

[Serializable]
public class ArrayTileType : Array<TileType>
{
	public ArrayTileType(int size) : base(size){}
}