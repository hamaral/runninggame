using System;

[Serializable]
public class ArrayInt : Array<int>
{
	public ArrayInt(int size) : base(size){}
}