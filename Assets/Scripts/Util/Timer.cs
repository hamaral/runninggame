﻿using UnityEngine;
using System;

public class Timer : MonoBehaviour
{
	private System.Action callback;
	public bool flushOnPurge = true;
	public const string kPurgeAllTimers = "PurgeTimers";
	public System.Action onLateUpdate;
	public System.Action onUpdate;
	private bool pauseOnTime;
	private Func<bool> specialPauseConditions;
	private float t;

	public void AddSpecialPauseCondition(Func<bool> callback)
	{
		this.specialPauseConditions = (Func<bool>) Delegate.Combine(this.specialPauseConditions, callback);
	}

	public static Timer AttachToLateUpdate(System.Action callback)
	{
		Timer updater = Timer.updater;
		updater.onLateUpdate = (System.Action) Delegate.Combine(updater.onLateUpdate, callback);
		return Timer.updater;
	}

	public static Timer AttachToUpdate(System.Action callback)
	{
		Timer updater = Timer.updater;
		updater.onUpdate = (System.Action) Delegate.Combine(updater.onUpdate, callback);
		return Timer.updater;
	}

	public static Timer CreateModalTimer(float time, System.Action callback)
	{
		Timer timer = CreateTimer(time, callback);
		timer.pauseOnTime = false;
		return timer;
	}

	public static Timer CreateTimer(float time, System.Action callback)
	{
		Timer timer = new GameObject("Timer").AddComponent<Timer>();
		timer.StartTimer(time, callback);
		return timer;
	}

	public void Destroy()
	{
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void FlushTimer()
	{
		if (this.callback != null)
		{
			this.callback();
		}
		this.StopTimer();
	}

	public float GetTimer() {
		return this.t;
	}

	private void LateUpdate()
	{
		if (this.onLateUpdate != null)
		{
			this.onLateUpdate();
		}
	}

	private void OnDisable()
	{
		this.onUpdate = null;
		this.onLateUpdate = null;
		MessageCenter.RemoveListener("PurgeTimers", new Action<Message>(this.PurgeTimer));
	}

	private void PurgeTimer(Message m)
	{
		if ((this != _updater) && this.flushOnPurge)
		{
			if (m.GetParameter<bool>("Flush"))
			{
				this.FlushTimer();
			}
			else
			{
				this.StopTimer();
			}
		}
	}

	public void Start()
	{
		MessageCenter.AddListener("PurgeTimers", new Action<Message>(this.PurgeTimer));
	}

	private void StartTimer(float time, System.Action call)
	{
		this.callback = call;
		this.t = time;
	}

	public void StopTimer()
	{
		this.callback = null;
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public static void UnAttach(System.Action callback)
	{
		Timer updater = Timer.updater;
		updater.onUpdate = (System.Action) Delegate.Remove(updater.onUpdate, callback);
		Timer timer2 = Timer.updater;
		timer2.onLateUpdate = (System.Action) Delegate.Remove(timer2.onLateUpdate, callback);
	}

	private void Update()
	{
		if ((!this.pauseOnTime || !this.specialPauseConditions()))
		{
			if (this.onUpdate != null)
			{
				this.onUpdate();
			}
			if (this.callback != null)
			{
				if (this.t <= 0f)
				{
					this.FlushTimer();
				}
				else
				{
					this.t -= Time.deltaTime;
				}
			}
		}
	}

	private static Timer _updater
	{
		
		get {
			return _updater;
		}

		set
		{
			_updater = value;
		}
	}

	private static Timer updater
	{
		get
		{
			if (_updater == null)
			{
				_updater = new GameObject("Updater") { hideFlags = HideFlags.HideAndDontSave }.AddComponent<Timer>();
			}
			return _updater;
		}
	}
}

