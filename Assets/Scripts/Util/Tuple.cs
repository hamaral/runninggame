﻿using System;
using System.Collections.Generic;

public class Tuple 
{
	public static Tuple<T1, T2> Create<T1, T2>(T1 item1, T2 item2) { 
		return new Tuple<T1, T2> (item1, item2);
	}

}

public class Tuple<T1, T2>
{

	public T1 Item1 { get; set; }

	public T2 Item2 { get; set; }

	public Tuple(T1 item1, T2 item2)
	{
		this.Item1 = item1;
		this.Item2 = item2;
	}

	public override bool Equals(object obj)
	{
		Tuple<T1, T2> tuple = obj as Tuple<T1, T2>;
		if (tuple == null)
		{
			return false;
		}
		return (EqualityComparer<T1>.Default.Equals(this.Item1, tuple.Item1) && EqualityComparer<T2>.Default.Equals(this.Item2, tuple.Item2));
	}

	public override int GetHashCode() {
		return (((this.Item1 != null) ? this.Item1.GetHashCode() : 1) ^ ((this.Item2 != null) ? this.Item2.GetHashCode() : 2));
	}

	public override string ToString() { 
		return "[{this.Item1}, {this.Item2}]";
	}


//	public static Tuple<T1, T2, T3> Create<T1, T2, T3>(T1 item1, T2 item2, T3 item3) { 
//		return new Tuple<T1, T2, T3> (item1, item2, item3);
//	}
}

