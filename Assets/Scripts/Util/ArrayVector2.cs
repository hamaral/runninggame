using System;
using UnityEngine;

[Serializable]
public class ArrayVector2 : Array<Vector2>
{
	public ArrayVector2(int size) : base(size){}
}