﻿using System;
using UnityEngine;

[Serializable]
public class Int2 
{

	public int x;
	public int y;

	public Int2(Int2 i)
	{
		this.x = i.x;
		this.y = i.y;
	}

	public Int2(int x0, int y0)
	{
		this.x = x0;
		this.y = y0;
	}

	public static explicit operator Vector2(Int2 int2a) { 
		return new Vector2 ((float)int2a.x, (float)int2a.y);
	}

	public static Int2 operator +(Int2 int2a, Int2 int2b) { 
		return new Int2 (int2a.x + int2b.x, int2a.y + int2b.y);
	}

	public static Int2 operator -(Int2 int2a, Int2 int2b) {
		return new Int2 (int2a.x - int2b.x, int2a.y - int2b.y);
	}

	public override string ToString() { 
		return "[{" + this.x + "}, {" + this.y + "}] ";
		
	}
}

