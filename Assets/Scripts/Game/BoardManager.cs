﻿using UnityEngine;
using System.Collections;

public class BoardManager : Singleton<BoardManager> {

	public int totalBlocks = 1;
	public TileType[,] m_Tiles;
	public ArrayTileType[]  m_Grid;
	public ArrayVector2[] m_GridPos;

	AStar m_aStar;

	void Awake()
	{
		//this.m_Tiles = GetComponentsInChildren<TileType>();
	}
	// Use this for initialization
	void Start () {
		this.m_aStar = new AStar();
		this.m_aStar.SetGrid(this.m_Grid);

		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnDrawGizmos() {
       // Gizmos.color = Color.red;		
       // Gizmos.DrawWireCube(this.m_GridPos, this.m_GridSize);
    }

	public NodePath GeneratePath(Vector3 start, Vector3 end)
	{
		Vector2 startNode = FindNode(start);
		Vector2 endNode = FindNode(end); 
		NodePath p = this.m_aStar.Search(startNode, endNode);

		return p;
	}

	Vector2 FindNode(Vector3 pos)
	{
		
		for(int i = 0; i < this.m_Grid.Length; i++)
		{
			for(int j = 0; j < this.m_Grid[i].Length; j++)
			{
				if(this.m_Grid[i][j].transform.position.x == pos.x && this.m_Grid[i][j].transform.position.y == pos.y)
				{
					return new Vector2(i, j);
				}
			}	
		}

		return new Vector2(-1, -1);
	}

	public ArrayTileType[] Grid
	{
		get {return this.m_Grid;}
		set {this.m_Grid = value;}
	}

	public ArrayVector2[] GridPos
	{
		get {return this.m_GridPos;}
		set {this.m_GridPos = value;}
	}
	
}
