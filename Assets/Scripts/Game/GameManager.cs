﻿using UnityEngine;
using System.Collections.Generic;

public class GameManager : Singleton<GameManager> {

	private List<ArrayInt>  grid = new List<ArrayInt>();

	private int m_TotalSurvivorRescued = 0;

	void Awake()
	{
		
	}


	
	public void PlaySoundToZombies(Transform instigator)
	{
		GameObject[] enemies =  GameObject.FindGameObjectsWithTag("Enemy");
		foreach (GameObject e in enemies) 
		{
			Zombie z = e.GetComponent<Zombie>();
			float dist = Vector3.Distance(instigator.position, z.transform.position);
			if(dist <= z.HearDistance)
			{
				z.HearSound(instigator);
			}
		}
	}

	public void RescuedSurvivor()
	{
		m_TotalSurvivorRescued++;
	}

	public int TotalSurvivorRescued
	{
		get {return this.m_TotalSurvivorRescued; }
	}
	public List<ArrayInt> Grid
	{
		get { return grid; }
	}
}
