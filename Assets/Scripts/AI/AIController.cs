﻿using UnityEngine;
using System.Collections;

public class AIController : MonoBehaviour {

	protected CharacterController2D m_Controller;
	[SerializeField]protected float m_TimeWait = 0.5f;

	private Vector3 m_Point;

	protected NodePath m_Path;
	private Transform m_Target;

	protected bool m_IsMoving;


	[SerializeField]private string m_EnemyTag;


	void Awake()
    {
        this.m_Controller = base.GetComponent<CharacterController2D>();
        
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void MoveTo(NodePath p)
	{
		if(p != null && !m_IsMoving)
		{
			this.m_Path = p;
			StartCoroutine(MoveToPoint());
		}
		
		
	}

	IEnumerator MoveToPoint()
    {

		if(this.m_Path != null && this.m_Path.Waypoints.Count > 0)
		{
			m_IsMoving = true;
			BoardManager board = BoardManager.Instance;
			float t = 0.0f;
			Vector2 point = this.m_Path.NextWaypoint();
			int m_totalPaths = 0;
			while (m_totalPaths < this.m_Path.Waypoints.Count)
			{
				if(transform.position.x == board.Grid[(int)point.x][(int)point.y].transform.position.x &&
					transform.position.y == board.Grid[(int)point.x][(int)point.y].transform.position.y)
				{
					this.m_Path.AdvanceToNext();
					point = this.m_Path.NextWaypoint();
					t = 0.0f;
					m_totalPaths++;
				}

				Vector3 dir = board.Grid[(int)point.x][(int)point.y].transform.position - base.transform.position;
				float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
				this.m_Controller.RotateBody(Quaternion.AngleAxis(angle, Vector3.forward));
				
				t += Time.deltaTime;
				transform.position = Vector2.Lerp(transform.position, board.Grid[(int)point.x][(int)point.y].transform.position, t/this.m_TimeWait);
				
				yield return new WaitForEndOfFrame();
			}
		}

		m_IsMoving = false;
		
	}

	IEnumerator AttackEnemy()
    {
		
		while (this.m_Target != null)
		{
			//rotacina o corpo
         //   this.m_Controller.RotateBody(Quaternion.LookRotation(Vector3.forward, this.m_Target.position - transform.position));
		 	Vector3 dir = this.m_Target.position - base.transform.position;
			float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
			this.m_Controller.RotateBody(Quaternion.AngleAxis(angle, Vector3.forward));
			this.m_Controller.FireWeapon();

			yield return null;
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.tag == this.m_EnemyTag) {
			this.m_Target = other.transform;
            StartCoroutine(AttackEnemy());        
		}
	}
    void OnTriggerStay2D(Collider2D other) {
        if(this.m_Target == null)
        {
            if(other.gameObject.tag == this.m_EnemyTag) {	
				this.m_Target = other.transform;		 
              	StartCoroutine(AttackEnemy());
            }
        }
    }

	private bool CanAttack()
    {
        if(this.m_Target != null)
        {
            float dist = Vector2.Distance(base.transform.position, this.m_Target.position);
                    
           
        }
        
        return false;
    }

	public Transform Target
	{
		get {return this.m_Target;}
		set {this.m_Target = value;}
	}

	public void ShootTarget(Transform t)
	{
		this.m_Target = t;
		StartCoroutine(AttackEnemy());
	}


}
