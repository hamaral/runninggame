using UnityEngine;
using System.Collections;

public class Zombie : AIController {

    [SerializeField]private float m_HearDistance = 10f;
        
    [SerializeField]private LayerMask m_LayerMask;


    void Awake()
    {
        this.m_Controller = base.GetComponent<CharacterController2D>();
        
    }
	// Use this for initialization
	void Start () {
       
	}	
	// Update is called once per frame
	void Update () {
	/*
        if(this.m_Target != null)
        {
            if(this.CanAttack()) {
                this.AttackEnemy();
            } else if(this.CanSeeEnemy(this.m_Target)){
                this.MoveToEnemy();
            }
        }
        */
	}

    IEnumerator AttackEnemy()
    {   
        if(CanAttack())
        {
            while (CanAttack())
            {
                base.m_Controller.FireWeapon();    
                yield return new WaitForEndOfFrame();
            }
        }
        else
        {
            BoardManager board = BoardManager.Instance;
            NodePath p = board.GeneratePath(base.m_Controller.transform.position, base.Target.position);
            this.m_Path = p;
            StartCoroutine(MoveToRangePoint());
        }
        
    }



    IEnumerator MoveToRangePoint()
    {

		if(base.m_Path != null && base.m_Path.Waypoints.Count > 0)
		{
			m_IsMoving = true;
			BoardManager board = BoardManager.Instance;
			float t = 0.0f;
			Vector2 point = this.m_Path.NextWaypoint();
			int m_totalPaths = 0;
			while (!this.CanAttack())
			{
				if(transform.position.x == board.Grid[(int)point.x][(int)point.y].transform.position.x &&
					transform.position.y == board.Grid[(int)point.x][(int)point.y].transform.position.y)
				{
					this.m_Path.AdvanceToNext();
					point = this.m_Path.NextWaypoint();
					t = 0.0f;
					m_totalPaths++;
				}

				Vector3 dir = board.Grid[(int)point.x][(int)point.y].transform.position - base.transform.position;
				float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
				this.m_Controller.RotateBody(Quaternion.AngleAxis(angle, Vector3.forward));
				
				t += Time.deltaTime;
				transform.position = Vector2.Lerp(transform.position, board.Grid[(int)point.x][(int)point.y].transform.position, t/this.m_TimeWait);
				
				yield return new WaitForEndOfFrame();
			}
		}
		m_IsMoving = false;
		StartCoroutine(this.AttackEnemy());
	}

    private bool CanAttack()
    {
        if(base.Target != null)
        {
            float dist = Vector2.Distance(base.transform.position, base.Target.position);

            if(this.CanSeeEnemy(base.Target) && dist <= base.m_Controller.Weapon.Range) {

                return true;
            }
        }
        
        return false;
    }

    private bool CanSeeEnemy(Transform other)
    {
        Vector3 direction = other.position - base.transform.position;
        float angle = Vector3.Angle(direction, base.transform.forward);
        
        if(angle < 110f )
        {
            RaycastHit2D hit = Physics2D.Raycast(base.transform.position, direction, 20f, m_LayerMask, -3f);                
            if(hit.collider != null && (hit.collider.gameObject.tag == "Player" || hit.collider.gameObject.tag == "Survivor"))
            {                
                return true;
            }
        }
        return false;
    }

	void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.tag == "Player" || other.gameObject.tag == "Survivor") {
			 
            if(this.CanSeeEnemy(other.transform)) {
                this.SeeEnemy( other.transform);
            }           
		}
	}
    void OnTriggerStay2D(Collider2D other) {
        if(base.Target == null)
        {
            if(other.gameObject.tag == "Player" || other.gameObject.tag == "Survivor") {
			 
               if(this.CanSeeEnemy(other.transform)) {
                    this.SeeEnemy( other.transform);
                }
            }
        }
    }


    public void HearSound(Transform instigator)
    {
        if(instigator.gameObject.tag == "Player" && base.Target == null)
        {
            base.Target = instigator;
            StartCoroutine(this.AttackEnemy());
        }
    }

    public void SeeEnemy(Transform instigator)
    {
        if(base.Target== null){
            base.Target = instigator;
            StartCoroutine(this.AttackEnemy());
        }
           
    }

    private void MoveToEnemy()
    {
        
        
    }


    public float HearDistance
    {
        get { return this.m_HearDistance; }
    }

}
