﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour {

	public GameObject m_ZombiePrefab;
	public int m_TotalZombiesPermit = 1;
	private int m_TotalZombie;

	// Use this for initialization
	void Start () {
		this.m_TotalZombie = 0;
		this.SpawnZombie();
	}
	
	public void SpawnZombie()
	{
		if(this.m_TotalZombie < this.m_TotalZombiesPermit){
			GameObject ob = Instantiate(this.m_ZombiePrefab) as GameObject;
			ob.transform.position = base.transform.position; 
			this.m_TotalZombie++;
		}
	}

	void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, .2f);
    }
}
