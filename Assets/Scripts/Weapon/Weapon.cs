using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {
    
    [SerializeField] private Projectile m_Projectile;
    [SerializeField] protected float m_FireRate = 0.5f;
    [SerializeField] protected Transform m_ShootPos;
    [SerializeField] protected float m_Damage = 5f;

    [SerializeField] protected float m_Range = 1f;

    protected Transform m_Owner;
    
    protected float m_LastFire = 0;

    protected void Awake()
    {
        m_Owner = base.transform.parent;
    }

    public virtual void Fire()
    {  
        if(m_FireRate <= (Time.time - this.m_LastFire) )
        {
            this.m_LastFire = Time.time;
            this.SpawnProjectile();
        }
    }
    
    private void SpawnProjectile()
    {
        SoundManager.Instance.PlaySingle(null, m_Owner);
        Projectile p =  Instantiate(m_Projectile, m_ShootPos.position, m_ShootPos.rotation) as Projectile;
        p.Init(this);
        
    }

    public float Damage
    {
        get {return this.m_Damage; }
    }

    public float FireRate
    {
        get{ return this.m_FireRate; }
    }

    public float Range
    {
        get{ return this.m_Range; }
    }
}