using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {
    
    [SerializeField]private float m_Speed = 1.0f;
    private Weapon m_Owner;
    

    public void Init(Weapon owner)
    {
        this.m_Owner = owner;
    }

    void Update()
    {
        this.transform.Translate(0, m_Speed, 0);
        
    }
    
    void OnCollisionEnter2D(Collision2D coll) {

       if (coll.gameObject.tag == "Enemy")
       {            
            CharacterController2D target = coll.gameObject.GetComponent<CharacterController2D>();
            if(target != null)
            {
                target.TakeDamage(this.m_Owner.Damage);
            }
       }
       else if (coll.gameObject.tag == "Door")
       {
            DoorTileType target = coll.gameObject.GetComponent<DoorTileType>();
            if(target != null)
            {
                target.TakeDamage(this.m_Owner.Damage);
            }
       }
       
       Destroy(gameObject);
        
    }
    
}