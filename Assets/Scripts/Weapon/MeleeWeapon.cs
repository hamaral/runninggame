using UnityEngine;
using System.Collections;

public class MeleeWeapon : Weapon {
    
   
    void Awake()
    {
        base.Awake();

    }

    void Start()
    {
        base.m_LastFire = 0;
    }

    public override void Fire()
    {  
        if(base.FireRate < (Time.time - base.m_LastFire) )
        {
            base.m_LastFire = Time.time;
            
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.left, 10f, 1 << LayerMask.NameToLayer("Character"));
            if(hit.collider != null && hit.collider.GetComponent<CharacterController2D>() != null)
            {
                hit.collider.GetComponent<CharacterController2D>().TakeDamage(base.Damage);
            }
            
        }
    }
    
 

}