using UnityEngine;
using UnityEditor;
using System;

enum E_DRAWOPTION {select, paint, paintover, erase};


[CustomEditor(typeof(BoardManager))]
public class TileMapEditor : Editor
{

    int  firstDimensionSize;
	int secondDimensionSize;
	bool editMode;
	E_DRAWOPTION selected;
	E_TileType tiletype;
	bool isEnabled;
	System.Random random = new System.Random();
	GameObject[] grassPrefabTypes;

	GameObject wallPrefabTypes;

	GameObject roomPrefabType;

	GameObject m_DoorPreabType;
	
    
    void OnEnable () {
		
		string[] lookFor = new string[] {"Assets/Prefabs/Tiles"};
		string[] guids2 = AssetDatabase.FindAssets ("tile_grama", lookFor);
		grassPrefabTypes = new GameObject[guids2.Length];
		int i = 0;
		foreach (string guid in guids2) {
			string assetPath = AssetDatabase.GUIDToAssetPath(guid);
			var asset = AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject)) as GameObject;
			grassPrefabTypes[i] = asset;
			i++;
		}

		i = 0;
		guids2 = AssetDatabase.FindAssets ("tile_parede_reta_tipo1", lookFor);
		//wallPrefabTypes = new GameObject[guids2.Length]
		
		foreach (string guid in guids2) {
			string assetPath = AssetDatabase.GUIDToAssetPath(guid);
			var asset = AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject)) as GameObject;
			wallPrefabTypes = asset;
			break;
		}

		i = 0;
		guids2 = AssetDatabase.FindAssets ("tile_chao_tipo1", lookFor);
		
		foreach (string guid in guids2) {
			string assetPath = AssetDatabase.GUIDToAssetPath(guid);
			var asset = AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject)) as GameObject;
			roomPrefabType  = asset;
			break;
		}

		guids2 = AssetDatabase.FindAssets ("tile_Door_tipo1", lookFor);
		
		foreach (string guid in guids2) {
			string assetPath = AssetDatabase.GUIDToAssetPath(guid);
			var asset = AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject)) as GameObject;
			m_DoorPreabType  = asset;
			break;
		}

		
	}

    public override void OnInspectorGUI()
	{
        DrawDefaultInspector();
        BoardManager someClass = (BoardManager)target;
        if(CanCreateNewArray()) CreateNewArray(someClass);

		GUILayout.BeginHorizontal();
		isEnabled = EditorGUILayout.Toggle(isEnabled, GUILayout.Width(16));
		selected = (E_DRAWOPTION)EditorGUILayout.EnumPopup(selected, GUILayout.Width(236));
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		tiletype = (E_TileType)EditorGUILayout.EnumPopup(tiletype, GUILayout.Width(236));
		GUILayout.EndHorizontal();
        SetupArray(someClass);

    }

 

    void OnSceneGUI() {
        BoardManager board = (BoardManager)target;
        
        Handles.BeginGUI();
        Handles.EndGUI();
        Vector3 minGrid = board.transform.position;
        for (int i = 0; i < board.GridPos.Length; i++ )
        {
             Handles.DrawLine(new Vector3(i, minGrid.y, 0.0f), new Vector3(i, board.GridPos.Length, 0.0f));
             for(int j = 0; j < board.GridPos[i].Length; j++)
             {

            	Handles.DrawLine(new Vector3(minGrid.x, i, 0.0f), new Vector3(board.GridPos[i].Length, i, 0.0f));
				
             }
      	}
		if(isEnabled)
		{
			Event e = Event.current;
			if (selected != E_DRAWOPTION.select)
			{
				HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
				if ((e.type == EventType.MouseDrag || e.type == EventType.MouseDown) && e.button == 0)
				{
					Vector2 mousePos = Event.current.mousePosition;
					mousePos.y = SceneView.currentDrawingSceneView.camera.pixelHeight - mousePos.y;
					Vector3 mouseWorldPos = SceneView.currentDrawingSceneView.camera.ScreenPointToRay(mousePos).origin;
					//mouseWorldPos.z = layerOrd;
					mouseWorldPos.x = Mathf.Floor(mouseWorldPos.x / 1) * 1 + 1/ 2.0f;
					mouseWorldPos.y = Mathf.Ceil(mouseWorldPos.y / 1) * 1 - 1 / 2.0f;
					
					GameObject[] allgo = GameObject.FindObjectsOfType(typeof (GameObject)) as GameObject[];
					int brk = 0;

					if (selected == E_DRAWOPTION.paint)
					{
						for (int i = 0; i < allgo.Length;i++)
						{
							if (Mathf.Approximately(allgo[i].transform.position.x, mouseWorldPos.x) && Mathf.Approximately(allgo[i].transform.position.y, mouseWorldPos.y) )
							{
								brk++;
								break;
							}
						}
						if(brk == 0)
						{
							DrawTile(mouseWorldPos);
						}
						
					}
					else if (selected == E_DRAWOPTION.erase)
					{
					}
				}
			}
    	}
	}

	
	public  Vector2 GetGridPosition(Vector2 worldPos)
	{
		 BoardManager board = (BoardManager)target;

		
		for (int i = 0; i < board.GridPos.Length; i++ )
        {
             for(int j = 0; j < board.GridPos[i].Length; j++)
             {

            	if(board.GridPos[i][j] == worldPos)
				{
					return new Vector2(i, j);
				}
				
             }
      	}
	
		return new Vector2(-1, -1);
	}

	GameObject GetRandomGrassTile()
	{	
		int index = random.Next(0, grassPrefabTypes.Length);
		return grassPrefabTypes[index];
	
	}

	void DrawTile(Vector2 worldPos)
	{
		Debug.Log("DrawTile");
		if(tiletype == E_TileType.grass)
		{
			AddTile(worldPos, GetRandomGrassTile());
			
		}
		else if(tiletype == E_TileType.wall)
		{
			AddTile(worldPos, wallPrefabTypes);
		}
		else if(tiletype == E_TileType.floor)
		{
			AddTile(worldPos, roomPrefabType);
		}
		else if(tiletype == E_TileType.door)
		{
			AddTile(worldPos, m_DoorPreabType);
		}
	}

	

	void AddTile(Vector2 worldPos, GameObject prefab, float angle = 0f)
	{
		BoardManager board = (BoardManager)target;
		
		GameObject newgo = PrefabUtility.InstantiatePrefab(prefab as GameObject) as GameObject;
		newgo.transform.position = worldPos;
		newgo.transform.SetParent(board.transform);
		if(angle != 0f)
		{
			newgo.transform.Rotate(new Vector3(0, 0, angle));
		}

		Vector2 pos = GetGridPosition(worldPos);
		board.Grid[(int)pos.x][(int)pos.y] = newgo.GetComponent<TileType>();
	}

	void RemoveTile(Vector2 worldPos, GameObject prefab)
	{
		BoardManager board = (BoardManager)target;
		
		Vector2 pos = GetGridPosition(worldPos);
		board.Grid[(int)pos.x][(int)pos.y] = null;
		DestroyImmediate(prefab);
	}

	void RotateTile(float angle, GameObject prefab)
	{
			prefab.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
	}

    bool CanCreateNewArray()
	{
		EditorGUILayout.BeginHorizontal();
		if(GUILayout.Button("Create New Array")) editMode = true;
		if(GUILayout.Button("Cancel")) editMode = false;
		EditorGUILayout.EndHorizontal();

		return editMode;
	}

    void CreateNewArray(BoardManager someClass)
	{
		GetDimensions();
		if(ConfirmedCanCreate()) CreateArray(someClass);
	}

    void CreateArray(BoardManager someClass)
	{
		someClass.GridPos = new ArrayVector2[firstDimensionSize];
		someClass.Grid = new ArrayTileType[firstDimensionSize];
		for(int i = 0; i < firstDimensionSize; i++)
		{
			someClass.GridPos[i] = new ArrayVector2(secondDimensionSize);
			someClass.Grid[i] = new ArrayTileType(secondDimensionSize);
		}

		for(int i = 0; i < someClass.GridPos.Length; i++)
		{
			for(int j = 0; j < someClass.GridPos[i].Length; j++)
			{
				someClass.GridPos[i][j] = new Vector2(i + 0.5f, j + 0.5f);
			}
		}
	}

	void GetDimensions()
	{
		firstDimensionSize = EditorGUILayout.IntSlider("Size X", firstDimensionSize, 10, 50);
        secondDimensionSize = EditorGUILayout.IntSlider("Size Y", secondDimensionSize, 10, 50);
	}

    void SetupArray(BoardManager someClass)
	{
       
        if(someClass.GridPos != null && someClass.GridPos.Length > 0)
		{
			for(int i = 0; i < someClass.GridPos.Length; i++)
			{
				GUILayout.BeginHorizontal();

				for(int j = 0; j < someClass.GridPos[i].Length; j++)
				{
				//	EditorGUI.DrawRect(new Rect(GUILayoutUtility.GetLastRect().x + 1f,
                  //                                      GUILayoutUtility.GetLastRect().y + 1f,
                    //                                    GUILayoutUtility.GetLastRect().width - 1f,
                      //                                  GUILayoutUtility.GetLastRect().height - 1f), Color.green);
					//someClass.map[i][j] = EditorGUILayout.IntField(someClass.map[i][j], GUILayout.Width(20));

					//EditorGUILayout.IntField(0, GUILayout.Width(20));
				}

				GUILayout.EndHorizontal();
			}
		}
		
    }


    bool ConfirmedCanCreate()
	{		

		EditorGUILayout.BeginHorizontal();
		bool canCreate = (GUILayout.Button("Create New Multidimensional Array"));
		EditorGUILayout.EndHorizontal();

		if(canCreate)
		{
 			editMode = false;
			return true;
		}
		return false;
	}
}